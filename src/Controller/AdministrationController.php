<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdministrationController extends Controller
{

    /**
     * @Route("/", name="APP")
     */
    public function APP()
    {
        $user= $this->get('security.token_storage')->getToken()->getUser();

        if(!$user instanceof User) return  $this->redirectToRoute('login_users');

        $roles=$user->getRoles();

        if(in_array("ROLE_ADMIN",$roles)){
            return  $this->redirectToRoute('administration');
        }elseif (in_array("ROLE_ETUDIANT",$roles)){
            return  $this->redirectToRoute('etudiant');
        }elseif (in_array("ROLE_PROF",$roles)){
            return  $this->redirectToRoute('professeur');
        }

    }

    /**
     * @Route("/welcome", name="welcome")
     */
    public function welcome()
    {
        $user= $this->get('security.token_storage')->getToken()->getUser();

        if(!$user instanceof User) return  $this->redirectToRoute('login_users');

        $roles=$user->getRoles();

        if(in_array("ROLE_ADMIN",$roles)){
            return  $this->redirectToRoute('administration');
        }elseif (in_array("ROLE_ETUDIANT",$roles)){
            return  $this->redirectToRoute('etudiant');
        }elseif (in_array("ROLE_PROF",$roles)){
            return  $this->redirectToRoute('professeur');
        }

    }


    /**
     * @Route("/administration", name="administration")
     */
    public function index()
    {

        return $this->render('administration/index.html.twig', [
            'controller_name' => 'AdministrationController',
        ]);
    }





    /**
     * @Route("/etudiantPanel", name="etudiant")
     */
    public function indexEtudiant()
    {
        return $this->render('etudiant/index.html.twig', [
            'controller_name' => 'AdministrationController',
        ]);
    }

    /**
     * @Route("/profsseurPanel", name="professeur")
     */
    public function indexProf()
    {
        return $this->render('professeur/index.html.twig', [
            'controller_name' => 'AdministrationController',
        ]);
    }
}
