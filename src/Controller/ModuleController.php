<?php

namespace App\Controller;

use App\Entity\Filiere;
use App\Entity\FiliereModule;
use App\Entity\Module ;
use App\Form\ModuleType;
use App\Utils\SerializerUtil;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/module")
 */

class ModuleController extends Controller
{
    /**
     * @Route("/new", name="module_new" , methods="GET|POST" )
     */
    public function ajout(Request $request)
    {
        $module=new Module();
        $form=$this->createForm(ModuleType::class,$module);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($module);
            $em->flush();
            return $this->redirectToRoute("module_list");
        }

        return $this->render('module/ajout.html.twig', [
            'controller_name' => 'ModuleController',
            'form'=>$form->createView(),

        ]);
    }

    /**
     * @Route("/list", name="module_list")
     */
    public function list()
    {

        $modules=$this->getDoctrine()->getRepository(Module::class)->findAll();
        return $this->render('module/list.html.twig', [
            'controller_name' => 'moduleController',
            'modules'=>$modules
        ]);
    }

    /**
     * @Route("/edit/{id}", name="module_edit" , methods="GET|POST" )
     */
    public function edit(Request $request , Module $module)
    {
        $form=$this->createForm(ModuleType::class,$module);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute("module_list");
        }

        return $this->render('module/edit.html.twig', [
            'module' => $module,
            'form'=>$form->createView(),

        ]);

    }


    /**
     * @Route("/delete/{id}/{token}", name="module_delete" , methods="GET|DELETE")
     */
    public function delete(Request $request , Module $fm , $token)
    {
        if ($this->isCsrfTokenValid('delete'.$fm->getId(),$token)) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($fm);
            $em->flush();
        }
        return $this->redirectToRoute('module_list');
    }

    /**
     * @Route("/delete/filiere/{id}/{token}", name="module_filiere_delete" , methods="GET|DELETE")
     */
    public function deleteFiliereModule(Request $request , FiliereModule $fm , $token )
    {

        if ($this->isCsrfTokenValid('delete'.$fm->getId(),$token)) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($fm);
            $em->flush();
        }
        return $this->redirectToRoute('filiere_index');
    }

    /**
     * @Route("/filiere/{id}", name="module_filiere" , methods="GET")
     */
    public function getModuleByFiliere(Filiere $filiere,SerializerUtil $ser,Request $request)
    {
        $modules = $this->getDoctrine()
            ->getRepository(Module::class)
            ->findAllModuleByFiliere($filiere);
        if($request->isXmlHttpRequest()){

            echo json_encode($modules); die() ;
        }

        return $this->render('module/list_module.html.twig', ['modules' => $modules]);
    }




}
