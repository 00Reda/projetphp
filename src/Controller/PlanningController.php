<?php

namespace App\Controller;

use App\Entity\Affectation;
use App\Entity\Filiere;
use App\Entity\Module;
use App\Entity\Planning;
use App\Form\PlanningType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/planning")
 */
class PlanningController extends Controller
{

    /**
     * @Route("/service/prof/list/{fid}/{mid}", name="planning_list_services_prof", methods="GET")
     */
    public function PlanninglistServiceProf(Request $request ): Response
    {
        $response=new Response();
        $fid=(int)$request->get("fid");
        $filiere=$this->getDoctrine()->getRepository(Filiere::class)->find($fid);
        $mid=(int)$request->get("mid");
        $result=[];
        if($mid!=-1){

            $module=$this->getDoctrine()->getRepository(Module::class)->find($mid);

            if($filiere==null or $module==null){

                $response->setStatusCode(404);
                echo json_encode(["errors"=>"link invalid"]);
                return $response;
            }

            $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
            $qb ->select('p')
                ->from(Planning::class, 'p')
                ->where("p.module=?1 and p.filiere=?2")
                ->setParameters([1=>$module ,2=>$filiere ]);
            $result = $qb->getQuery()->execute();

        }else{

            if($filiere==null ){

                $response->setStatusCode(404);
                echo json_encode(["errors"=>"link invalid"]);
                return $response;
            }

            $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
            $qb ->select('p')
                ->from(Planning::class, 'p')
                ->where("p.filiere=?1")
                ->setParameter(1,$filiere);
            $result = $qb->getQuery()->execute();

        }

        $json=[];

        foreach ($result as $elm){
            $json[]=$elm->ToJSON();
        }
         $response->setStatusCode(200);
        echo json_encode($json);
        return $response;


    }

    /**
     * @Route("/service/etudiant/list/{fid}", name="planning_list_services_etudiant", methods="GET")
     */
    public function PlanninglistServiceEtudiant(Request $request ): Response
    {
        $response=new Response();
        $fid=(int)$request->get("fid");
        $filiere=$this->getDoctrine()->getRepository(Filiere::class)->find($fid);


            if($filiere==null ){

                $response->setStatusCode(404);
                echo json_encode(["errors"=>"link invalid"]);
                return $response;
            }

            $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
            $qb ->select('p')
                ->from(Planning::class, 'p')
                ->where("p.filiere=?1")
                ->setParameter(1,$filiere);
            $result = $qb->getQuery()->execute();



        $json=[];

        foreach ($result as $elm){
            $json[]=$elm->ToJSON();
        }
        $response->setStatusCode(200);
        echo json_encode($json);
        return $response;


    }

    /**
     * @Route("/prof", name="planning_prof_filiere", methods="GET")
     */
    public function listProfFiliere(UserRepository $rep): Response
    {
        $user= $this->get('security.token_storage')->getToken()->getUser();;
        $filieres = $rep->findUserAffectations($user);

        return $this->render('planning/prof_filiere.html.twig', ['affectations' => $filieres]);
    }

    /**
     * @Route("/{id}", name="planning_index", methods="GET")
     */
    public function index(Affectation $aff): Response
    {

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->select('p')
            ->from(Planning::class, 'p')
            ->where("p.module=?1 and p.filiere=?2")
            ->setParameters([1=>$aff->getModule() ,2=>$aff->getFiliere() ]);
        $plannings = $qb->getQuery()->execute();
        return $this->render('planning/index.html.twig', ['plannings' => $plannings,"affectation"=>$aff]);
    }

    /**
     * @Route("/etudiant/list", name="planning_etudiant", methods="GET")
     */
    public function indexEtudiant(): Response
    {
        /* $documents = $this->getDoctrine()
             ->getRepository(Document::class)
             ->findAll();*/
        $user= $this->get('security.token_storage')->getToken()->getUser();

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->select('p')
            ->from(Planning::class, 'p')
            ->where(" p.filiere=?1")
            ->setParameter(1,$user->getFiliere());
        $result = $qb->getQuery()->execute();

        return $this->render('planning/index_etudiant.html.twig', ['plannings' => $result ,]);
    }

    /**
     * @Route("/new/{id}", name="planning_new", methods="GET|POST")
     */
    public function new(Affectation $aff,Request $request): Response
    {
        $planning = new Planning();
        $form = $this->createForm(PlanningType::class, $planning);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $planning->setModule($aff->getModule());
            $planning->setFiliere($aff->getFiliere());
            $em->persist($planning);
            $em->flush();

            return $this->redirectToRoute('planning_index' ,["id"=>$aff->getId()]);
        }

        return $this->render('planning/new.html.twig', [
            "affectation"=>$aff,
            'planning' => $planning,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}/{idaff}", name="planning_show", methods="GET")
     */
    public function show(Request $request): Response
    {
        $affid=(int)$request->get("idaff");
        $aff=$this->getDoctrine()->getRepository(Affectation::class)->find($affid);
        $pid=(int)$request->get("id");
        $planning=$this->getDoctrine()->getRepository(Planning::class)->find($pid);
        return $this->render('planning/show.html.twig', ['planning' => $planning,'affectation'=>$aff,]);
    }

    /**
     * @Route("/{id}/edit/{idaff}", name="planning_edit", methods="GET|POST")
     */
    public function edit(Request $request): Response
    {
        $affid=(int)$request->get("idaff");
        $aff=$this->getDoctrine()->getRepository(Affectation::class)->find($affid);
        $pid=(int)$request->get("id");
        $planning=$this->getDoctrine()->getRepository(Planning::class)->find($pid);

        $form = $this->createForm(PlanningType::class, $planning);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('planning_index', ['id' => $aff->getId()]);
        }

        return $this->render('planning/edit.html.twig', [
            'planning' => $planning,
            'affectation'=>$aff,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{idaff}", name="planning_delete", methods="DELETE")
     */
    public function delete(Request $request): Response
    {
        $affid=(int)$request->get("idaff");
        $aff=$this->getDoctrine()->getRepository(Affectation::class)->find($affid);
        $pid=(int)$request->get("id");
        $planning=$this->getDoctrine()->getRepository(Planning::class)->find($pid);

        if ($this->isCsrfTokenValid('delete'.$planning->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($planning);
            $em->flush();
        }

        return $this->redirectToRoute('planning_index', ['id' => $aff->getId()]);
    }
}
