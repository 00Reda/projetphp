<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="note", indexes={@ORM\Index(name="id_etudiant", columns={"id_etudiant"}), @ORM\Index(name="id_filiere", columns={"id_filiere"}), @ORM\Index(name="id_module", columns={"id_module"})})
 * @ORM\Entity
 */
class Note
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="note", type="float", precision=10, scale=0, nullable=false)
     */
    private $note;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_etudiant", referencedColumnName="iduser")
     * })
     */
    private $etudiant;





    /**
     * @var \Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_module", referencedColumnName="ID")
     * })
     */


    private $module;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getNote(): ?float
    {
        return $this->note;
    }

    /**
     * @param float $note
     */
    public function setNote(float $note): void
    {
        $this->note = $note;
    }

    /**
     * @return \User
     */
    public function getEtudiant(): ?User
    {
        return $this->etudiant;
    }

    /**
     * @param \User $etudiant
     */
    public function setEtudiant(User $etudiant): void
    {
        $this->etudiant = $etudiant;
    }



    /**
     * @return \Module
     */
    public function getModule(): ?Module
    {
        return $this->module;
    }

    /**
     * @param \Module $module
     */
    public function setModule(Module $module): void
    {
        $this->module = $module;
    }






}
