<?php

namespace App\Repository;

use App\Entity\Affectation;
use App\Entity\Filiere;
use App\Entity\Note;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class UserRepository extends BaseRepository implements UserLoaderInterface
{

    public function __construct(RegistryInterface $doctrine)
    {
        $this->class_name = User::class;
        parent::__construct($doctrine);
    }

    /**
     * Loads the user for the given username.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @return UserInterface|null
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $role
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function loadUserByRole($role){
        $conn = $this->getEntityManager()->getConnection();

        $sql = "    SELECT * FROM user m 
                      where iduser IN ( select user_iduser from role , user_has_role
                                        where user_has_role.role_idrole=role.idrole and role.role= '".$role."'"
            .');';


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_CLASS,'App\Entity\User');
        $data=$stmt->fetchAll();



        echo "<pre>";
        var_dump($data);die();
    }


    public function findUserAffectations(User $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb ->select('aff')
            ->from(Affectation::class, 'aff')
            ->where("aff.user=?1")
            ->setParameter(1,$user);
        $result = $qb->getQuery()->execute();
        return $result;

    }
    public function findUserNotes(User $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb ->select('n')
            ->from(Note::class, 'n')
            ->where("n.etudiant=?1")
            ->setParameter(1,$user);
        $result = $qb->getQuery()->execute();
        return $result;

    }



}