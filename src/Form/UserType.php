<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname',null , array('required'=>true))
            ->add('lastname',null , array('required'=>true))
            ->add('username',null , array('required'=>true))
            ->add('email',EmailType::class , array('required'=>true))
            ->add('isactive',null , array('required'=>true)) ;

        if(isset($options["attr"]) && $options["attr"]==[true]){

            $builder->add('password',PasswordType::class  , array('required'=>false));

        }else{
            $builder->add('password',PasswordType::class  , array('required'=>true));
        }

        $builder->add('userroles',null , array('required'=>false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
