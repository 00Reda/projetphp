<?php


namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;



class RoleRepository extends BaseRepository// extends BaseRepository
{
    public function __construct(RegistryInterface $doctrine)
    {
        $this->class_name = Role::class;
        parent::__construct($doctrine);
    }

    public function addUserRole($userID){
        $conn = $this->getEntityManager()->getConnection();

        $sql = '  insert into user_has_role values ('.$userID.' , 2 , NULL);';


        $stmt = $conn->prepare($sql);
        $res = $stmt->execute();

    }
}