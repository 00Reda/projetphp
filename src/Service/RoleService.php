<?php

namespace App\Service;


use App\Entity\Role;
use App\Entity\User;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RoleService extends BaseService
{

    public function __construct(RoleRepository $rolerepo,TokenStorageInterface $token)
    {
        parent::__construct($rolerepo,$token);
    }

    public function createRole($name,$role)
    {
        $role = new Role($name,$role);
        $this->save($role);
    }

    public function removeRole($id)
    {
        $role = $this->get($id);
        if(!$role){
            throw new \Exception();
        }
        $this->repo->remove($role);
    }

    public function updateRole($id,$name,$srole)
    {
        /* @var Role $role */
        $role = $this->get($id);
        if(!$role){
            throw new \Exception();
        }
        $role->setName($name);
        $role->setRole($srole);

        $this->update($role);
    }

    public function getRole($roleName)
    {
        return $this->getOneByRole($roleName);
    }


}