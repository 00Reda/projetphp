<?php


namespace App\Utils;

use App\Entity\Genres;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerUtil
{


    private $encoders;
    private $normalizers ;
    private $serializer ;

    public function __construct()
    {
        $this->encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            if($object instanceof User)
            {
                return $object->getIduser();
            }
        });


        $dateCallBack = function($object){
            if($object instanceof \DateTime)
            {
                return $object->format("Y-m-d");
            }
            return $object;
        };

        $genreCallback = function($object){
            if($object instanceof Genres)
            {
                return $object->getNom();
            }
        };


        $normalizer->setCallbacks(array(
            "du" => $dateCallBack,
            "au" => $dateCallBack,
            "genre"=>$genreCallback
        ));

        $this->normalizers = array($normalizer);

    }

    /**
     * @return String
     */
    public function toJSON($object,$context=[])
    {
        if($object != NULL)
        {
            if(is_object($object))
            {
                $this->normalizers[0]->setIgnoredAttributes($object->getIgnored());
            }elseif (count($object)>0){
                $this->normalizers[0]->setIgnoredAttributes($object[0]->getIgnored());
            }
        }
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
        return $this->serializer->serialize($object, 'json',$context);
    }

}
