<?php

namespace App\Controller;

use App\Service\RoleService;
use App\Utils\SerializerUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RoleController
 * @package App\Controller
 *
 * @Route("/roles")
 *
 */
class RoleController implements BaseController
{

    private $role_service ;
    private $serializer;

    /**
     * RoleController constructor.
     */
    public function __construct(RoleService $role_service , SerializerUtil $serializer)
    {
        $this->role_service = $role_service;
        $this->serializer = $serializer;
    }
    /**
     *
     * @Route("/list" , name="list_roles" )
     *
     */
    public function getAll()
    {
        $jsonContent = $this->serializer->toJSON($this->role_service->getAll());
        return new Response($jsonContent,200,array("Content-Type"=>"application/json"));
    }
    /**
     *
     * @Route("/create" , name="create_role" )
     *
     */
    public function create(Request $request)
    {
        $name = $request->request->get('name');
        $role = $request->request->get('role');
        $this->role_service->createRole($name,$role);
        return new JsonResponse("Role Created Successfully");
    }
    /**
     *
     * @Route("/update" , name="update_role" )
     *
     */
    public function update(Request $request)
    {
        $id = $request->request->get("id");
        $name = $request->request->get('name');
        $role = $request->request->get('role');
        $this->role_service->updateRole($id,$name,$role);
        return new JsonResponse("Role Updated Successfully");
    }
    /**
     *
     * @Route("/remove/{id}" , name="remove_role" )
     *
     */
    public function remove($id)
    {
        $this->role_service->removeRole($id);
        return new Response("Role Removed Successfully");
    }
}