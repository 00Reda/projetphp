<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{




    /**
     * @var string|null
     *
     * @ORM\Column(name="firstName", type="string", length=45, nullable=true)
     */
    private $firstname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lastName", type="string", length=45, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=45, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=false)
     */
    private $email;


    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=false)
     */
    private $password;

    /**
     * @var bool
     *
     * @ORM\Column(name="isActive", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isactive = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="iduser", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iduser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", inversedBy="user")
     * @ORM\JoinTable(name="user_has_role",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_iduser", referencedColumnName="iduser")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="role_idrole", referencedColumnName="idrole")
     *   }
     * )
     */
    private $userroles;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Filiere", inversedBy="etudiant")
     * @ORM\JoinTable(name="inscription",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_etudiant", referencedColumnName="iduser")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_filiere", referencedColumnName="ID")
     *   }
     * )
     */
    private $filiere;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userroles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->filiere=new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * @return null|string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }



    /**
     * @param null|string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return null|string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param null|string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): ? string
    {

        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(?string $password)
    {
        if($password!=null)
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function isIsactive():? bool
    {
        return $this->isactive;
    }

    /**
     * @param bool $isactive
     */
    public function setIsactive(bool $isactive)
    {
        $this->isactive = $isactive;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }


    /**
     * @return int
     */
    public function getIduser(): ?int
    {
        return $this->iduser;
    }

    /**
     * @param int $iduser
     */
    public function setIduser(int $iduser)
    {
        $this->iduser = $iduser;
    }





    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoles(): \Doctrine\Common\Collections\Collection
    {
        return $this->userroles;
    }
    /**
     * @param \Doctrine\Common\Collections\Collection $userroles
     */
    public function setUserRoles(\Doctrine\Common\Collections\Collection $userroles)
    {

        $this->userroles = $userroles;
    }


    /**
     * @param Collection $f
     */
    public function setFiliere($f)
    {

        if($f instanceof Filiere){
            $col=new \Doctrine\Common\Collections\ArrayCollection();
            $col->add($f);
            $this->filiere=$col;
        }else{
            $this->filiere = $f;
        }

    }

    /**
     * @return Collection
     */
    public function getFiliere() : ?Filiere
    {
        $one= $this->filiere->first();
        if($one instanceof Filiere) return $one ;
        else return null;
    }
    /**
     * @return Filiere
     */


    public function getFilieres()
    {
        // TODO: Implement getRoles() method.
        $f = [];
        foreach($this->filiere as $elm)
        {
            $f[] = $elm->getNom();
        }
        //var_dump($f);die();
        return $f;
    }








    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->iduser,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
            //$this->isactive
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->iduser,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            //$this->isactive
            ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        // TODO: Implement getRoles() method.
        $roles = [];
        foreach($this->userroles as $role)
        {
            $roles[] = $role->getRole();
        }

        return $roles;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        // TODO: Implement isAccountNonExpired() method.
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        // TODO: Implement isAccountNonLocked() method.
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        // TODO: Implement isCredentialsNonExpired() method.
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        // TODO: Implement isEnabled() method.
    }



    private $toIgnore = array("password","salt","ignored");
    public function getIgnored()
    {
        return $this->toIgnore;
    }

    public function getIsactive(): ?bool
    {
        return $this->isactive;
    }



    public function addUserrole(Role $userrole): self
    {
        if (!$this->userroles->contains($userrole)) {
            $this->userroles[] = $userrole;
        }

        return $this;
    }

    public function removeUserrole(Role $userrole): self
    {
        if ($this->userroles->contains($userrole)) {
            $this->userroles->removeElement($userrole);
        }

        return $this;
    }


    public function addFiliere(Filiere $f): self
    {
        if (!$this->filiere->contains($f)) {
            $this->filiere[] = $f;
        }

        return $this;
    }

    public function removeFiliere(Filiere $f): self
    {
        if ($this->filiere->contains($f)) {
            $this->filiere->removeElement($f);
        }

        return $this;
    }

}
