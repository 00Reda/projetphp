<?php

namespace App\Controller;

use App\Entity\Absence;
use App\Form\AbsenceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/absence")
 */
class AbsenceController extends Controller
{
    /**
     * @Route("/", name="absence_index", methods="GET")
     */
    public function index(): Response
    {
        $absences = $this->getDoctrine()
            ->getRepository(Absence::class)
            ->findAll();

        return $this->render('absence/index.html.twig', ['absences' => $absences]);
    }

    /**
     * @Route("/new", name="absence_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $absence = new Absence();
        $form = $this->createForm(AbsenceType::class, $absence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($absence);
            $em->flush();

            return $this->redirectToRoute('absence_index');
        }

        return $this->render('absence/new.html.twig', [
            'absence' => $absence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="absence_show", methods="GET")
     */
    public function show(Absence $absence): Response
    {
        return $this->render('absence/show.html.twig', ['absence' => $absence]);
    }

    /**
     * @Route("/{id}/edit", name="absence_edit", methods="GET|POST")
     */
    public function edit(Request $request, Absence $absence): Response
    {
        $form = $this->createForm(AbsenceType::class, $absence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('absence_edit', ['id' => $absence->getId()]);
        }

        return $this->render('absence/edit.html.twig', [
            'absence' => $absence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="absence_delete", methods="DELETE")
     */
    public function delete(Request $request, Absence $absence): Response
    {
        if ($this->isCsrfTokenValid('delete'.$absence->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($absence);
            $em->flush();
        }

        return $this->redirectToRoute('absence_index');
    }
}
