<?php


namespace App\Service;


use App\Entity\User;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route(service="UserAService")
 */
class UserService extends BaseService
{

    private $encoder;
    private $roleService;

    public function __construct(UserRepository $userRepo , UserPasswordEncoderInterface $encoder , RoleService $roleService, TokenStorageInterface $token)
    {
        parent::__construct($userRepo,$token);
        $this->encoder = $encoder;
        $this->roleService = $roleService;
    }

    public function createUser($first_name , $last_name , $password  ,$user_name, $email)
    {
        $u = new User();

        $role = $this->roleService->getRole("ROLE_USER");
        $encoded = $this->encoder->encodePassword($u, $password);
        $roles = $u->getUserRoles();
        $roles->add($role);

        $u->setFirstname($first_name);
        $u->setLastname($last_name);
        $u->setPassword($encoded);
        $u->setEmail($email);
        $u->setUserRoles($roles);
        $u->setUsername($user_name);

        return $this->save($u);
    }

    public function removeUser($id)
    {
        $user = $this->repo->find($id);
        if(!$user){
            throw new \Exception();
        }
        $this->repo->remove($user);
    }

    public function updateUser($id ,$first_name , $last_name , $password  ,$user_name, $email)
    {
        /* @var User $user */
        $user = $this->repo->find($id);
        if(!$user){
            throw new \Exception();
        }
        $user->setFirstname($first_name);
        $user->setLastname($last_name);
        $user->setEmail($email);
        $user->setUsername($user_name);
        $password = $encoded = $this->encoder->encodePassword($user, $password);
        $user->setPassword($password);

        $this->update($user);
    }


}