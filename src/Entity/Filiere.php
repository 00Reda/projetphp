<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Filiere
 *
 * @ORM\Table(name="filiere")
 * @ORM\Entity
 */
class Filiere
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="filiere")
     * @ORM\JoinTable(name="inscription",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_filiere", referencedColumnName="ID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_etudiant", referencedColumnName="iduser")
     *   }
     * )
     */


    private $etudiant ;


    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;





    public function __construct()
    {
        $this->etudiant = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getEtudiants()
    {
        // TODO: Implement getRoles() method.
        $etud = [];
        foreach($this->etudiant as $e)
        {
            $etud[] = $e->getUsername().":".$e->getLastname().":".$e->getFirstname();
        }
        return $etud;
    }
    public function getEtudiant()
    {
        // TODO: Implement getRoles() method.
       return $this->etudiant;
    }


    /**
     * @param \Doctrine\Common\Collections\Collection $etudiant
     */
    public function setEtudiant(\Doctrine\Common\Collections\Collection $etudiant): void
    {
        $this->etudiant = $etudiant;
    }


    public function addEtudiant(User $u): self
    {
        if (!$this->etudiant->contains($u)) {
            $this->etudiant[] = $u;
        }

        return $this;
    }

    public function removeUserrole(User $u): self
    {
        if ($this->etudiant->contains($u)) {
            $this->etudiant->removeElement($u);
        }

        return $this;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }




    public function __toString()
    {
        return $this->nom;
    }


}
