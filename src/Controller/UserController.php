<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use App\Form\EtudiantType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/user")
 */
class UserController extends Controller
{

    /**
     *
     * @Route("/login" , name= "login_users")
     *
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $user= $this->get('security.token_storage')->getToken()->getUser();

        if($user!=null and $user instanceof User){
            $roles=$user->getRoles();
            if(in_array("ROLE_ADMIN",$roles)){
                return  $this->redirectToRoute('administration');
            }elseif (in_array("ROLE_ETUDIANT",$roles)){
                return  $this->redirectToRoute('etudiant');
            }elseif (in_array("ROLE_PROF",$roles)){
                return  $this->redirectToRoute('professeur');
            }
        }

        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/", name="user_index", methods="GET")
     */
    public function index(UserRepository $userRepository): Response
    {

        return $this->render('user/index.html.twig', ['users' => $userRepository->findAll()]);
    }
    /**
     * @Route("/list/etudiant", name="user_index_etudiant", methods="GET")
     */
    public function indexEtudiant(UserRepository $userRepository): Response
    {

        return $this->render('user/index_etudiant.html.twig', ['users' => $userRepository->findAll()]);
    }
    /**
     * @Route("/list/prof", name="user_index_prof", methods="GET")
     */
    public function indexProf(UserRepository $userRepository): Response
    {

        return $this->render('user/index_prof.html.twig', ['users' => $userRepository->findAll()]);
    }

    /**
     * @Route("/list/admin", name="user_index_admin", methods="GET")
     */
    public function indexAdmin(UserRepository $userRepository): Response
    {

        return $this->render('user/index_admin.html.twig', ['users' => $userRepository->findAll()]);
    }

    /**
     * @Route("/etudiant/new", name="etudiant_new", methods="GET|POST")
     */
    public function newEtudiant(Request $request , UserPasswordEncoderInterface $encoder ): Response
    {
        $user = new User();
        $form = $this->createForm(EtudiantType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $em = $this->getDoctrine()->getManager();

            try{
                $em->persist($user);

                $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

                $em->flush();

               // $this->getDoctrine()->getRepository(Role::class)->addUserRole($user->getIduser());

            }catch(\Exception $e){

                return $this->render('etudiant/new.html.twig', ['error' => [$e->getMessage()] , 'form' => $form->createView(),]);
            }


            return $this->redirectToRoute('user_index');
        }

        return $this->render('etudiant/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/etudiant/{iduser}/edit", name="etudiant_edit", methods="GET|POST")
     */
    public function editEtudiant(Request $request, User $user , UserPasswordEncoderInterface $encoder): Response
    {
        $options=["attr"=>[true]];

        $form = $this->createForm(EtudiantType::class, $user,$options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try{
                if(strlen($user->getPassword())<60)
                    $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
                    $this->getDoctrine()->getManager()->flush();
            }catch(\Exception $e){
                return $this->render('etudiant/edit.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'error' => [$e->getMessage()]
                ]);
            }



            return $this->redirectToRoute('etudiant', ['iduser' => $user->getIduser(),['user' => $user]]);
        }
        return $this->render('etudiant/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods="GET|POST")
     */
    public function new(Request $request , UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            try{
                $em->persist($user);
                $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
                $em->flush();
            }catch(\Exception $e){

                return $this->render('user/new.html.twig', ['error' => [$e->getMessage()] , 'form' => $form->createView(),]);
             }


            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{iduser}", name="user_show", methods="GET")
     */
    public function show(User $user): Response
    {

        return $this->render('user/show.html.twig', ['user' => $user]);
    }

    /**
     * @Route("/{iduser}/edit", name="user_edit", methods="GET|POST")
     */
    public function edit(Request $request, User $user , UserPasswordEncoderInterface $encoder): Response
    {
        $options=["attr"=>[true]];

        $form = $this->createForm(UserType::class, $user,$options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try{
                if(strlen($user->getPassword())<60)
                   $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

                $this->getDoctrine()->getManager()->flush();
            }catch(\Exception $e){
                return $this->render('user/edit.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'error' => [$e->getMessage()]
                ]);
            }



            return $this->redirectToRoute('user_show', ['iduser' => $user->getIduser(),['user' => $user]]);
        }
        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{iduser}/edit/etudiant", name="user_edit_etudiant", methods="GET|POST")
     */
    public function editEtudiantAdministration(Request $request, User $user , UserPasswordEncoderInterface $encoder): Response
    {
        $options=["attr"=>[null]];

        $form = $this->createForm(EtudiantType::class, $user,$options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try{
                if(strlen($user->getPassword())<60)
                    $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

                $this->getDoctrine()->getManager()->flush();
            }catch(\Exception $e){
                return $this->render('user/edit.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'error' => [$e->getMessage()]
                ]);
            }



            return $this->redirectToRoute('user_index_etudiant', ['iduser' => $user->getIduser(),['user' => $user]]);
        }
        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/{iduser}", name="user_delete", methods="DELETE")
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getIduser(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
