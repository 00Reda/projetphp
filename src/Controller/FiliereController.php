<?php

namespace App\Controller;

use App\Entity\Filiere;
use App\Entity\FiliereModule;
use App\Entity\Module;
use App\Form\AffectationModuleType;
use App\Form\FiliereType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/filiere")
 */
class FiliereController extends Controller
{
    /**
     * @Route("/", name="filiere_index", methods="GET")
     */
    public function index(): Response
    {
        $filieres = $this->getDoctrine()
            ->getRepository(Filiere::class)
            ->findAll();
         //var_dump($filieres[0]->getEtudiant()[0]);die();
        return $this->render('filiere/index.html.twig', ['filieres' => $filieres]);
    }

    /**
     * @Route("/new", name="filiere_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $filiere = new Filiere();
        $form = $this->createForm(FiliereType::class, $filiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($filiere);
            $em->flush();

            return $this->redirectToRoute('filiere_index');
        }

        return $this->render('filiere/new.html.twig', [
            'filiere' => $filiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="filiere_show", methods="GET")
     */
    public function show(Filiere $filiere): Response
    {

        return $this->render('filiere/show.html.twig', ['filiere' => $filiere]);
    }

    /**
     * @Route("/etudiants/{id}", name="filiere_etudiant", methods="GET")
     */
    public function listEtudiant(Filiere $filiere): Response
    {

        return $this->render('filiere/etudiant.html.twig', ['filiere' => $filiere]);
    }

    /**
     * @Route("/{id}/edit", name="filiere_edit", methods="GET|POST")
     */
    public function edit(Request $request, Filiere $filiere): Response
    {
        $form = $this->createForm(FiliereType::class, $filiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('filiere_index');
        }

        return $this->render('filiere/edit.html.twig', [
            'filiere' => $filiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="filiere_delete", methods="DELETE")
     */
    public function delete(Request $request, Filiere $filiere): Response
    {
        if ($this->isCsrfTokenValid('delete'.$filiere->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($filiere);
            $em->flush();
        }

        return $this->redirectToRoute('filiere_index');
    }


    /*
     * @Route("/module/{id}", name="filiere_module", methods="GET")
     */

    /*
    public function getFiliere(Request $request, Module $module){
        $filieres = $this->getDoctrine()
            ->getRepository(FiliereModule::class)
            ->findAll();

        return $this->render('filiere/index.html.twig', ['filieres' => $filieres]);
    }
    */



    /**
     * @Route("/affectation/{id}", name="affectation_module", methods="GET|POST")
     */


    public function affectationModule(Request $request, Filiere $filiere){

        $modules = $this->getDoctrine()
            ->getRepository(Module::class)
            ->findAllModuleNonAffecterFiliere($filiere);

        $options=["data"=>$modules ];
        $form = $this->createForm(AffectationModuleType::class, null, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $fm=new FiliereModule();
            $modules=$request->request->get('affectation_module')["modules"];
            $semestre=(int)$request->request->get('affectation_module')["semestre"];

            foreach ($modules as $elm ){

                $fm->setIdFiliere($filiere);

                $module = $this->getDoctrine()
                    ->getRepository(Module::class)
                    ->findOneBy(array('id'=>(int)$elm));

                $fm->setIdModule($module);

                $fm->setSemestre($semestre);



                $em = $this->getDoctrine()->getManager();
                $em->persist($fm);
                $em->flush();

            }



            return $this->redirectToRoute('filiere_index');
        }

        return $this->render('filiere/affectation_module.html.twig', [
            'filiere' => $filiere ,
             "form" => $form->createView(),
        ]);
    }




}
