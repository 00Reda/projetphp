$(function() {

    // page is now ready, initialize the calendar...
    var showCalendar=function(data){
        $('#calendar').fullCalendar( {
            // put your options and callbacks here
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            locale:"fr" ,
            weekends:true ,
            editable:false ,

            events: data ,
        });
        $("#myCalendrier").modal();
    }

    var AjaxLoad=function(elm){

        var urlS= elm.attr("data-href");
        var res=[];

        $.ajax({
            url : urlS,
            type : 'GET', // Le type de la requête HTTP, ici devenu POST
            dataType : 'json',
            success : function(code_json, statut){ // code_html contient le HTML renvoyé
                if(statut==200){
                    console.log("ok")
                }
            },
            error: function (resultat , statu ,error) {

                alert("eereur lors du chargement du planning ressayer !");

            } ,
            complete:function (resultat, statu ,res) {

                res=resultat.responseJSON;


                  showCalendar(res);

            }

        });

        return res;

    }


    $('#popup').on("click",function (e) {
                e.preventDefault();

                AjaxLoad($('#popup'));



    });

});

