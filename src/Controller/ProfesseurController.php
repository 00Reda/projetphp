<?php
/**
 * Created by PhpStorm.
 * User: IdeaPad
 * Date: 2018-06-12
 * Time: 6:47 PM
 */

namespace App\Controller;
use App\Entity\Affectation;
use App\Entity\Filiere;
use App\Entity\FiliereModule;
use App\Entity\Module;
use App\Entity\Note;
use App\Entity\User;
use App\Form\AffectationModuleType;
use App\Form\FiliereType;
use App\Form\NoteType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/prof")
 */

class ProfesseurController extends Controller
{
    /**
     * @Route("/note/filiere", name="prof_filiere", methods="GET")
     */
    public function index(UserRepository $rep): Response
    {
        $user= $this->get('security.token_storage')->getToken()->getUser();;
        $filieres = $rep->findUserAffectations($user);

        return $this->render('professeur/prof_filiere.html.twig', ['affectations' => $filieres]);
    }

    /**
     * @Route("/affectation/etudiant/{id}", name="prof_etudiant", methods="GET")
     */
    public function Etud(UserRepository $rep , Affectation $aff): Response
    {

        return $this->render('professeur/etudiant_filiere.html.twig', ['affectation' => $aff]);
    }

    /**
     * @Route("/donnernote/{id}/{iduser}", name="prof_give_note", methods="GET|POST")
     */
    public function giveNote(UserRepository $rep , Affectation $aff ,Request $request): Response
    {
        $userid=$request->get("iduser");
        $user=$this->getDoctrine()->getRepository(User::class)->find($userid);

        $note = new Note();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $note->setEtudiant($user);
            $note->setModule($aff->getModule());


            $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
            $qb ->select('note')
                ->from(Note::class, 'note')
                ->where("note.module=?1 and note.etudiant=?2")
                ->setParameters([1=>$aff->getModule() ,2=>$user ]);
            $result = $qb->getQuery()->execute();

           if($result !==[]){
               return $this->render('note/new.html.twig', [
                   'affectation' => $aff,
                   "etudiant"=>$user,
                   'form' => $form->createView(),
                   "error"=>["la note du module ".$aff->getModule()->getNom().'et deja affecte a l\'utilisateur '.$user->getFirstname()." ".$user->getLastname() ." penser a la modifiee ."],
               ]);
           }

            try{
                $em->persist($note);
                $em->flush();
            }catch (\Exception $e){
                return $this->render('note/new.html.twig', [
                    'affectation' => $aff,
                    "etudiant"=>$user,
                    'form' => $form->createView(),
                    "error"=>[$e->getMessage()],
                ]);

            }

            return $this->redirectToRoute('prof_etudiant' ,["id"=>$aff->getId()]);
        }


        return $this->render('note/new.html.twig', [
            'affectation' => $aff,
            "etudiant"=>$user,
            'form' => $form->createView(),

        ]);


    }

    /**
     * @Route("/listnote/{id}", name="prof_list_note", methods="GET")
     */
    public function listNote(Request $request): Response
    {
        $moduleID=(int)$request->get("id");
        $module=$this->getDoctrine()->getRepository(Module::class)->find($moduleID);

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->select('note')
            ->from(Note::class, 'note')
            ->where("note.module=?1")
            ->setParameter(1,$module);
        $result = $qb->getQuery()->execute();

        return $this->render('note/index.html.twig', [
            'module' => $module,
            'notes'=>$result,


        ]);

    }


}