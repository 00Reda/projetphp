<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FiliereModule
 *
 * @ORM\Table(name="filiere_module", indexes={@ORM\Index(name="id_filiere", columns={"id_filiere"}), @ORM\Index(name="id_module", columns={"id_module"})})
 * @ORM\Entity
 */
class FiliereModule
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="semestre", type="integer", nullable=false)
     */
    private $semestre;

    /**
     * @var \Filiere
     *
     * @ORM\ManyToOne(targetEntity="Filiere" , cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_filiere", referencedColumnName="ID")
     * })
     */
    private $idFiliere;

    /**
     * @var \Module
     *
     * @ORM\ManyToOne(targetEntity="Module" , cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_module", referencedColumnName="ID")
     * })
     */
    private $idModule;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSemestre(): ?int
    {
        return $this->semestre;
    }

    public function setSemestre(int $semestre): self
    {
        $this->semestre = $semestre;

        return $this;
    }

    public function getIdFiliere(): ?Filiere
    {
        return $this->idFiliere;
    }

    public function setIdFiliere(?Filiere $idFiliere): self
    {
        $this->idFiliere = $idFiliere;

        return $this;
    }

    public function getIdModule(): ?Module
    {
        return $this->idModule;
    }

    public function setIdModule(?Module $idModule): self
    {
        $this->idModule = $idModule;

        return $this;
    }


}
