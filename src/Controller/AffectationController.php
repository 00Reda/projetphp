<?php
/**
 * Created by PhpStorm.
 * User: IdeaPad
 * Date: 2018-06-11
 * Time: 5:42 PM
 */

namespace App\Controller;


use App\Entity\Affectation;
use App\Entity\Filiere;
use App\Entity\Module;
use App\Entity\User;

use App\Form\AffectationProfesseurType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affectation")
 */
class AffectationController extends Controller
{

    /**
     * @Route("/list/prof/{iduser}" , name="affectation_prof" , methods="GET" )
     *
     */

    public function list( Request $request , User $user , UserRepository $repository){

          //affectations
        $affectations = $repository->findUserAffectations($user);

        return $this->render('affectation/index.html.twig', ['affectations' => $affectations,"user"=>$user]);
    }


    /**
     * @Route("/new/{iduser}", name="affectation_new", methods="GET|POST")
     */
    public function new(Request $request,User $user)
    {


        $aff = new Affectation();
        $post=$request->get("add");
        if (isset($post)) {

            $semestre=(int)$request->get("semestre");
            $filiereID=(int)$request->get("filiere");
            $moduleID=(int)$request->get("module");

            $filiere=$this->getDoctrine()->getRepository(Filiere::class)->find($filiereID);
            $module=$this->getDoctrine()->getRepository(Module::class)->find($moduleID);
            //var_dump($module->getNom());die();
            if($filiere==null or $module==null){
                return $this->render('affectation/new.html.twig', [
                    'affectation' => $aff,
                    'filiere' => $this->getDoctrine()->getRepository(Filiere::class)->findAll(),
                    "user"=>$user,
                    "error"=>["invalid module ou filiere !"],
                ]);
            }

            $em = $this->getDoctrine()->getManager();

            try{

                $aff->setUser($user);
                $aff->setModule($module);
                $aff->setFiliere($filiere);
                $aff->setSemestre($semestre);
                $em->persist($aff);
                $em->flush();
                return $this->redirectToRoute('affectation_prof',["iduser"=>$user->getIduser()]);
            }catch (\Exception $e){
                return $this->render('affectation/new.html.twig', [
                    'affectation' => $aff,
                    'filiere' => $this->getDoctrine()->getRepository(Filiere::class)->findAll(),
                    "user"=>$user,
                    "error"=>[$e->getMessage()],
                ]);
            }



        }

        return $this->render('affectation/new.html.twig', [
            'affectation' => $aff,
            'filiere' => $this->getDoctrine()->getRepository(Filiere::class)->findAll(),
            "user"=>$user,
        ]);
    }


    /**
     * @Route("delete/{id}/{token}", name="affectation_delete", methods="DELETE|GET")
     */
    public function delete(Request $request, Affectation $aff , $token)
    {
        if ($this->isCsrfTokenValid('delete'.$aff->getId(),$token)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($aff);
            $em->flush();
        }

        return $this->redirectToRoute('affectation_prof',["iduser"=>$aff->getUser()->getIduser()]);
    }
}