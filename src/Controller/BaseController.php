<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;

interface BaseController
{
    public function getAll();
    public function create(Request $request);
    public function update(Request $request);
    public function remove($id);
}