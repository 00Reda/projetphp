<?php
/**
 * Created by PhpStorm.
 * User: IdeaPad
 * Date: 2018-06-06
 * Time: 4:32 AM
 */

namespace App\Repository;


use App\Entity\Filiere;
use App\Entity\Module;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Module::class);
    }

    /**
     * @param Filiere $filiere
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findAllModuleByFiliere(Filiere $filiere){

        $conn = $this->getEntityManager()->getConnection();

        $sql = '      SELECT m.id as module_id , f.semestre as semestre , m.nom as nom , f.id as id , f.id_filiere as filiere_id
                      FROM module m , filiere_module f where m.id=f.id_module
                      and f.id_filiere= '.$filiere->getId();

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findAllModuleNonAffecterFiliere(Filiere $filiere){

        $conn = $this->getEntityManager()->getConnection();

        $sql = '      SELECT m.id as id , m.nom as nom FROM module m 
                      where id NOT IN ( select id_module from filiere_module where id_filiere= '.$filiere->getId()
                        .');';


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data=$stmt->fetchAll();
        $res =[];

        foreach ($data as $elm){
               $res[$elm["nom"]]=$elm["id"];
        }

        return $res;
    }


}