<?php


namespace App\Controller;

use App\Utils\SerializerUtil;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use App\Service\UserService;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 *
 * @Route("/users")
 *
 */
class UsersController extends Controller implements BaseController
{

    private $user_service;
    private $serializer;

    public function __construct(UserService $user_service, SerializerUtil $serializer)
    {
        $this->user_service = $user_service;
        $this->serializer = $serializer;
    }

  /*
     *
     * @Route("/login" , name= "login_user")
     *
     */
  /*
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }*/



    /**
     *
     * @Route("/list" , name= "list_users")
     *
     */
    public function getAll()
    {
        $jsonContent = $this->serializer->toJSON($this->user_service->getAll());
        return new Response($jsonContent,200,array("Content-Type"=>"application/json"));
    }
    /**
     *
     * @Route("/create" , name= "create_user")
     *
     */
    public function create(Request $request)
    {
        $first_name = $request->request->get("first_name");
        $last_name = $request->request->get("last_name");
        $password = $request->request->get("password");
        $username = $request->request->get("username");
        $email = $request->request->get("email");

        $this->user_service->createUser($first_name, $last_name, $password, $username, $email);
        return new Response("User Created Succefully");
    }
    /**
     *
     * @Route("/remove/{id}" , name= "remove_user")
     *
     */
    public function remove($id)
    {
        $this->user_service->removeUser($id);
        return new Response("User Removed Succefully");
    }
    /**
     *
     * @Route("/update" , name= "update_user")
     *
     */
    public function update(Request $request)
    {
        $id = $request->request->get('id');
        $first_name = $request->request->get("first_name");
        $last_name = $request->request->get("last_name");
        $password = $request->request->get("password");
        $username = $request->request->get("username");
        $email = $request->request->get("email");
        $this->user_service->updateUser($id , $first_name , $last_name , $password ,$username , $email);
        return new Response("User Updated Succefully");
    }

}
