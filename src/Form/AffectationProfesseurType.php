<?php
namespace App\Form;

use App\Entity\Filiere;
use App\Entity\Module;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\ChoiceToValueTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AffectationProfesseurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('filiere' , null , array(
                "attr"=>["id"=>"filiere"],
                "multiple"=>false ,
                "required"=>true,
            ))
        ;
        /*$builder
            ->add('module' , null , array(
                "attr"=>["id"=>"module"],
                "multiple"=>false,
                "required"=>true,
                "data"=>[]
            ))
        ;*/

        $builder->add('semestre' , IntegerType::class , array(
            'attr'=>array(
                'min'=>1 ,
                'max'=>50,
                "required"=>true,
            )
        )) ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        /*$resolver->setDefaults([
            'data_class' => Filiere::class,
        ]);*/
    }
}