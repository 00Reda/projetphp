<?php

namespace App\Controller;

use App\Entity\Affectation;
use App\Entity\Document;
use App\Form\DocumentType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/document")
 */
class DocumentController extends Controller
{



    /**
     * @Route("/prof", name="document_prof_filiere", methods="GET")
     */
    public function listProfFiliere(UserRepository $rep): Response
    {
        $user= $this->get('security.token_storage')->getToken()->getUser();;
        $filieres = $rep->findUserAffectations($user);

        return $this->render('document/prof_filiere.html.twig', ['affectations' => $filieres]);
    }



    /**
     * @Route("/{id}", name="document_index", methods="GET")
     */
    public function index(Affectation $aff): Response
    {
       /* $documents = $this->getDoctrine()
            ->getRepository(Document::class)
            ->findAll();*/

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->select('doc')
            ->from(Document::class, 'doc')
            ->where("doc.module=?1 and doc.filiere=?2")
            ->setParameters([1=>$aff->getModule() ,2=>$aff->getFiliere() ]);
        $documents = $qb->getQuery()->execute();
        return $this->render('document/index.html.twig', ['documents' => $documents ,"affectation"=>$aff]);
    }

    /**
     * @Route("/etudiant/list", name="document_etudiant", methods="GET")
     */
    public function indexEtudiant(): Response
    {
        /* $documents = $this->getDoctrine()
             ->getRepository(Document::class)
             ->findAll();*/
        $user= $this->get('security.token_storage')->getToken()->getUser();

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->select('doc')
            ->from(Document::class, 'doc')
            ->where(" doc.filiere=?2")
            ->setParameter(2,$user->getFiliere());
        $documents = $qb->getQuery()->execute();
        return $this->render('document/index_etudiant.html.twig', ['documents' => $documents ,]);
    }

    /**
     * @Route("/new/{id}", name="document_new", methods="GET|POST")
     */
    public function new(Request $request , Affectation $aff): Response
    {
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $document->setModule($aff->getModule());
            $document->setFiliere($aff->getFiliere());

            try{
                $em->persist($document);
                $em->flush();
            }catch (\Exception $e){
                return $this->render('document/new.html.twig', [
                    'document' => $document,
                    "affectation"=>$aff,
                    'form' => $form->createView(),
                    "error"=>[$e->getMessage()]
                ]);
            }


            return $this->redirectToRoute('document_index', ['id' => $aff->getId()]);
        }

        return $this->render('document/new.html.twig', [
            'document' => $document,
            "affectation"=>$aff,
            'form' => $form->createView(),
        ]);
    }

    /**
     */
    public function show(Request $request): Response
    {
        $docid=(int)$request->get("id");
        $affid=(int)$request->get("idaff");
        $document=$this->getDoctrine()->getRepository(Document::class)->find($docid);
        $aff=$this->getDoctrine()->getRepository(Affectation::class)->find($affid);
        return $this->render('document/show.html.twig', ['document' => $document , "affectation"=>$aff]);
    }

    /**
     * @Route("/{id}/edit/{idaff}", name="document_edit", methods="GET|POST")
     */
    public function edit(Request $request): Response
    {
        $docid=(int)$request->get("id");
        $affid=(int)$request->get("idaff");
        $document=$this->getDoctrine()->getRepository(Document::class)->find($docid);
        $aff=$this->getDoctrine()->getRepository(Affectation::class)->find($affid);

        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $this->getDoctrine()->getManager()->flush();
            }catch (\Exception $e){

                return $this->render('document/edit.html.twig', [
                    'document' => $document,
                    "affectation"=>$aff,
                    'form' => $form->createView(),
                    "error"=>[$e->getMessage()]
                ]);
            }


            return $this->redirectToRoute('document_index', ['id' => $aff->getId()]);
        }

        return $this->render('document/edit.html.twig', [
            'document' => $document,
            "affectation"=>$aff,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/delete/{id}/{idaff}", name="document_delete", methods="DELETE")
     */
    public function delete(Request $request): Response
    {
        $docid=(int)$request->get("id");
        $affid=(int)$request->get("idaff");
        $document=$this->getDoctrine()->getRepository(Document::class)->find($docid);
        $aff=$this->getDoctrine()->getRepository(Affectation::class)->find($affid);

        if ($this->isCsrfTokenValid('delete'.$document->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($document);
            $em->flush();
        }

        return $this->redirectToRoute('document_index', ['id' => $aff->getId()]);
    }
}
