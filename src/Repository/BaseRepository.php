<?php


namespace App\Repository;


use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseRepository extends ServiceEntityRepository
{

    protected $class_name;

    public function __construct(RegistryInterface $doctrine)
    {
        parent::__construct($doctrine, $this->class_name);
    }

    public function save($obj)
    {
        $this->getEntityManager()->persist($obj);
        $this->getEntityManager()->flush();
    }

    public function remove($obj)
    {
        $this->getEntityManager()->remove($obj);
        $this->getEntityManager()->flush();
    }

    public function update($obj=null)
    {
        $this->getEntityManager()->flush();
    }

}