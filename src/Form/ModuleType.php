<?php
/**
 * Created by PhpStorm.
 * User: IdeaPad
 * Date: 2018-06-05
 * Time: 9:34 PM
 */
namespace App\Form;
use Symfony\Component\Form\AbstractType;

class ModuleType extends AbstractType{

   public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
   {

       $builder->add("nom");

   }

   public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
   {
       $resolver->setDefaults( array(
                                    "data_class"=>\App\Entity\Module::class ,
                                ));

   }

}
?>