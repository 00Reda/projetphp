<?php

namespace App\Service;


use App\Entity\User;
use App\Repository\BaseRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BaseService
{
    protected $repo;
    protected $token;

    public function __construct(BaseRepository $repo,TokenStorageInterface $token)
    {
        $this->repo = $repo;
        $this->token = $token;
    }

    public function get($id){
        return $this->repo->find($id);
    }

    public function getAll(){
        return $this->repo->findAll();
    }

    public function save($object){
        return $this->repo->save($object);
    }

    public function update($obj){
        $this->repo->update($obj);
    }

    public function __call($fctName,$arguments){
        $function = str_replace("get","find",$fctName);
        return call_user_func_array(array($this->repo, $function), $arguments);
    }

    public function remove($object){
        $this->repo->remove($object);
    }

}