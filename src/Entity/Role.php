<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Role
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=100, nullable=false)
     */
    private $role;

    /**
     * @var int
     *
     * @ORM\Column(name="idrole", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrole;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="userroles")
     *
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct($name,$role)
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->name = $name;
        $this->role = $role;

    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getIdrole(): ?int
    {
        return $this->idrole;
    }

    /**
     * @param int $idrole
     */
    public function setIdrole(int $idrole)
    {
        $this->idrole = $idrole;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser(): \Doctrine\Common\Collections\Collection
    {
        return $this->user;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $user
     */
    public function setUser(\Doctrine\Common\Collections\Collection $user)
    {
        $this->user = $user;
    }


    private $toIgnore = array("user","ignored");
    public function getIgnored()
    {
        return $this->toIgnore;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->addUserrole($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            $user->removeUserrole($this);
        }

        return $this;
    }


    public function __toString() {
        return $this->name;
    }


}
