<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Planning
 *
 * @ORM\Table(name="planning", indexes={@ORM\Index(name="id_filiere", columns={"id_filiere"}), @ORM\Index(name="id_module", columns={"id_module"})})
 * @ORM\Entity
 */
class Planning
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jour", type="date", nullable=false)
     */
    private $jour;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from_date", type="datetime", nullable=false)
     */
    private $fromDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="to_date", type="datetime", nullable=false)
     */
    private $toDate;


    /**
     * @var \Filiere
     *
     * @ORM\ManyToOne(targetEntity="Filiere")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_filiere", referencedColumnName="ID")
     * })
     */
    private $filiere;

    /**
     * @var \Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_module", referencedColumnName="ID")
     * })
     */
    private $module;

    /**
     * @var \string
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     */

    private $description;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return null|void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getJour(): ?\DateTime
    {
        return $this->jour;
    }

    /**
     * @param \DateTime $jour
     * @return null|void
     */
    public function setJour(\DateTime $jour): void
    {
        $this->jour = $jour;
    }

    /**
     * @return \DateTime
     */
    public function getFromDate(): ?\DateTime
    {
        return $this->fromDate;
    }

    /**
     * @param \DateTime $fromDate
     */
    public function setFromDate(\DateTime $fromDate): void
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return \DateTime
     */
    public function getToDate(): ?\DateTime
    {
        return $this->toDate;
    }

    /**
     * @param \DateTime $toDate
     */
    public function setToDate(\DateTime $toDate): void
    {
        $this->toDate = $toDate;
    }

    /**
     * @return \Filiere
     */
    public function getFiliere(): ?Filiere
    {
        return $this->filiere;
    }

    /**
     * @param \Filiere $filiere
     */
    public function setFiliere(Filiere $filiere): void
    {
        $this->filiere = $filiere;
    }

    /**
     * @return \Module
     */
    public function getModule(): ?Module
    {
        return $this->module;
    }

    /**
     * @param \Module $module
     */
    public function setModule(Module $module): void
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function ToJSON(){
        $json=[];
        $json["title"]=$this->module->getNom()." : ".$this->description;

        $json["start"]=$this->jour->format("Y-m-d");
        $json["start"].=" ";
        $json["start"].=$this->fromDate->format("H:i:s");

        $json["end"]=$this->jour->format("Y-m-d");
        $json["end"].=" ";
        $json["end"].=$this->toDate->format("H:i:s");

        return $json;
    }






}
